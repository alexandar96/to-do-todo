<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class TodoItemController extends Controller
{
    /**
     * Display create new form to the user
     * 
     * @return view
     */
    public function create()
    {
        
    }
    
    /**
     * Save new ToDo entry, redirect user to list view
     * 
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Fetch selected ToDo entry and display it on the view to edit existing ToDo entry
     * 
     * @param int $id
     * @return view
     */
    public function edit($id)
    {
        
    }

    /**
     * Save changes to existing toDo entry, redirect to list view
     * 
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Delete existing ToDo entry, redirect to list view
     * 
     * @param int $id
     * @return redirect
     */
    public function destroy($id)
    {
        TodoItem::destroy($id);

        return redirect('/');
    }
    
    /**
     * Mark existing ToDo entry as done, redirect to list view
     * 
     * @param Request $request
     * @return redirect
     */
    public function done(Request $request, $id)
    {
        
    }
    
    /**
     * Mark existing entry as not done, redirect to list view
     * 
     * @param Request $request
     * @return redirect
     */
    public function undo(Request $request, $id)
    {
        
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * Define editable fields and table name to which the model points to.
 * 
 * Table was created with following command or similar with same effect:
 * CREATE TABLE todos (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, content TEXT, done BOOL);
 *  
 */
class TodoItem extends Model
{
    
}
